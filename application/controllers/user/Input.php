<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input extends CI_Controller {
	 function __construct(){
    parent::__construct();
    $this->load->helper(array('url'));
    $this->load->model ('Model_laporan');
    $this->load->model ('Model_pelanggaran');
    $this->load->model('blog_model');
	error_reporting(0);

    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{
		if($this->session->userdata('akses')=='3'){
	$data['pelanggaran'] = $this->Model_pelanggaran->get();
	$data['kode_lapor'] = $this->Model_laporan->buat_kode();
	$this->load->vars($data);
    $this->load->view('user/input',$data);
    }else{
     $this->load->view('warning');
}
}
	function get_autocomplete(){
		if (isset($_GET['term'])) {
		  	$result = $this->blog_model->search_blog($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
					'label'			=> $row->nama,
					'description'	=> $row->nip,
				);
		     	echo json_encode($arr_result);
		   	}
		}
	}
    public function upload(){
		$config['upload_path']          = './assets/images/bukti/perorangan/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000;
		$config['max_width']            = 50000;
		$config['max_height']           = 50000;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			echo "gagal upload";

		}
		else{
			$img = $this->upload->data();
			$bukti = $img['file_name'];
			$id = $this->input->post('id');
			$kode_lapor = $this->input->post('kode_lapor');
			$tgl_lapor = $this->input->post('tgl_lapor');
			$pelanggaran = $this->input->post('pelanggaran');
			$tempat_kejadian = $this->input->post('tempat_kejadian');
			$tanggal = $this->input->post('tanggal');
			$uraian = $this->input->post('uraian');
			$status = $this->input->post('status');
			$nama = $this->input->post('nama');
			$nip = $this->input->post('nip');

			$data = array(
				'bukti' => $bukti,
				'id' => $id,
				'kode_lapor' => $kode_lapor,
				'tgl_lapor' => $tgl_lapor,
				'pelanggaran' => $pelanggaran,
				'tempat_kejadian' => $tempat_kejadian,
				'tanggal' => $tanggal,
				'uraian' => $uraian,
				'status' => $status,
				'nama' => $nama,
				'nip' => $nip,

			);
			$this->db->insert('laporan',$data);
			$this->load->view('user/data_perorangan',$data);
			redirect('user/data_perorangan');
		}
}
}
