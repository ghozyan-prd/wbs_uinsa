<?php
defined('BASEPATH') OR exit('No Direct Script allowed');

class data_unit extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Books_model_unit2');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{	if($this->session->userdata('akses')=='3'){
		$data['books'] = $this->Books_model_unit2->get_all_books();
		$this->load->view('non_civitas/data_unit',$data);
    }else{
    	$this->load->view('warning');
    }
		
	}
	
	public function book_add()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama_unit' 		=> $this->input->post('nama_unit'),
			'jenis_unit' 		=> $this->input->post('jenis_unit'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$insert = $this->Books_model_unit2->book_add($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit($id_laporan_unit)
	{
		$data = $this->Books_model_unit2->get_by_id($id_laporan_unit);
		echo json_encode($data);
	}

	public function book_update()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama_unit' 		=> $this->input->post('nama_unit'),
			'jenis_unit' 		=> $this->input->post('jenis_unit'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$this->Books_model_unit2->book_update(array('id_laporan_unit' => $this->input->post('id_laporan_unit')), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function book_delete($id_laporan_unit)
	{
		$this->Books_model_unit2->delete_by_id($id_laporan_unit);
		echo json_encode(array("status" => TRUE));
    }
}