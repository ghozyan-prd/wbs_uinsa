<?php
defined('BASEPATH') OR exit('No Direct Script allowed');

class jenis_pelanggaran extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('M_jenis_pelanggaran');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
	}

	public function index()
	{	if($this->session->userdata('akses')=='1'){
		$data['jenis_pelanggaran'] = $this->M_jenis_pelanggaran->get_all_books();
		$this->load->view('admin/jenis_pelanggaran',$data);
		 }else{
    	$this->load->view('warning');
    }
	}
	
	public function book_add()
	{
		$data = array(
			'kode_pelanggaran' 			=> $this->input->post('kode_pelanggaran'),
			'nama_pelanggaran'		=> $this->input->post('nama_pelanggaran'),
			'tgl' 		=> $this->input->post('tgl'),
			);
		$insert = $this->M_jenis_pelanggaran->book_add($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit($id_pelanggaran)
	{
		$data = $this->M_jenis_pelanggaran->get_by_id($id_pelanggaran);
		echo json_encode($data);
	}

	public function book_update()
	{
		$data = array(
			'kode_pelanggaran' => $this->input->post('kode_pelanggaran'),
			'nama_pelanggaran' => $this->input->post('nama_pelanggaran'),
			'tgl' => $this->input->post('tgl'),

			);
		$this->M_jenis_pelanggaran->book_update(array('id_pelanggaran' => $this->input->post('id_pelanggaran')), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function book_delete($id_pelanggaran)
	{
		$this->M_jenis_pelanggaran->delete_by_id($id_pelanggaran);
		echo json_encode(array("status" => TRUE));
	}
}