<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambah extends CI_Controller {
	 function __construct(){
    parent::__construct();
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{
		if($this->session->userdata('akses')=='1'){
      $this->load->view('admin/tambah');
    }else{
    	$this->load->view('warning');
    }
}
 public function upload(){
			$nip = $this->input->post('nip');
			$password = $this->input->post('password');
			$nama = $this->input->post('nama');
			$jabatan = $this->input->post('jabatan');
			$level = $this->input->post('level');

			$data = array(
				'nip' => $nip,
				'password' => $password,
				'nama' => $nama,
				'jabatan' => $jabatan,
				'level' => $level,
				
			);
			$this->db->insert('pengawas',$data);
			$this->load->view('admin/data_user');
			redirect('admin/data_user');
		}
}

