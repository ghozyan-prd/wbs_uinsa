<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BAP extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('pdf');
		$this->load->helper('url');
		$this->load->model('person_model','BAP');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
	}
}

	public function index()
	{	if($this->session->userdata('akses')=='1'){
		$this->load->view('admin/BAP');
    }else{
    	$this->load->view('warning');
    }
		
	}

	public function ajax_list()
	{
		$this->load->helper('url');

		$list = $this->BAP->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $BAP) {
			$no++;
			$row = array();
			$row[] = $BAP->kode_lapor;
			$row[] = $BAP->nama;
			$row[] = $BAP->kode_undangan;
			$row[] = $BAP->tgl_dibuat;
			$row[] = $BAP->tgl_undangan;
			$row[] = $BAP->pukul;
			if($BAP->bap)
				$row[] = '<a href="'.base_url('assets/images/BAP/'.$BAP->bap).'" target="_blank"><img src="'.base_url('assets/images/BAP/'.$BAP->bap).'" class="img-responsive" /></a>';
			else
				$row[] = '(No File Selected)';

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_BAP('."'".$BAP->id_undangan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
			<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="print" onclick="print_BAP('."'".$BAP->id_undangan."'".')"><i class="glyphicon glyphicon-print"></i> print</a>

				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_BAP('."'".$BAP->id_undangan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->BAP->count_all(),
						"recordsFiltered" => $this->BAP->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id_undangan)
	{
		$data = $this->BAP->get_by_id($id_undangan);
		$data->tgl_undangan = ($data->tgl_undangan == '0000-00-00') ? '' : $data->tgl_undangan; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->_validate();
		
		$data = array(
				'kode_lapor' => $this->input->post('kode_lapor'),
				'nama' => $this->input->post('nama'),
				'kode_undangan' => $this->input->post('kode_undangan'),
				'tgl_dibuat' => $this->input->post('tgl_dibuat'),
				'tgl_undangan' => $this->input->post('tgl_undangan'),
				'pukul' => $this->input->post('pukul'),
				'photo' => $this->input->post('photo'),
				
			);

		if(!empty($_FILES['bap']['name']))
		{
			$upload = $this->_do_upload();
			$data['bap'] = $upload;
		}

		$insert = $this->BAP->save($data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'kode_lapor' => $this->input->post('kode_lapor'),
				'nama' => $this->input->post('nama'),
				'kode_undangan' => $this->input->post('kode_undangan'),
				'tgl_dibuat' => $this->input->post('tgl_dibuat'),
				'tgl_undangan' => $this->input->post('tgl_undangan'),
				'pukul' => $this->input->post('pukul'),
				'photo' => $this->input->post('photo'),
			);

		if($this->input->post('remove_bap')) // if remove bap checked
		{
			if(file_exists('assets/images/BAP/'.$this->input->post('remove_bap')) && $this->input->post('remove_bap'))
				unlink('assets/images/BAP/'.$this->input->post('remove_bap'));
			$data['bap'] = '';
		}

		if(!empty($_FILES['bap']['name']))
		{
			$upload = $this->_do_upload();
			
			//delete file
			$BAP = $this->BAP->get_by_id($this->input->post('id_undangan'));
			if(file_exists('assets/images/BAP/'.$BAP->bap) && $BAP->bap)
				unlink('assets/images/BAP/'.$BAP->bap);

			$data['bap'] = $upload;
		}

		$this->BAP->update(array('id_undangan' => $this->input->post('id_undangan')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id_undangan)
	{
		//delete file
		$BAP = $this->BAP->get_by_id($id_undangan);
		if(file_exists('assets/images/BAP/'.$BAP->bap) && $BAP->bap)
			unlink('assets/images/BAP/'.$BAP->bap);
		
		$this->BAP->delete_by_id($id_undangan);
		echo json_encode(array("status" => TRUE));
	}

	public function cetak()
	{
		//delete file
		$id_undangan = $this->uri->segment(4);
		$this->db->from('undangan');
		$this->db->where('id_undangan',$id_undangan);
		$query = $this->db->get();
		$undangan = $query->result();
		foreach ($undangan as $row){
		
		$pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
		$pdf->AddPage();
		$pdf->Image('assets/images/logo2.png',10,4,27,27);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
		// mencetak string 
		$pdf->Cell(200,0,'KEMENTERIAN AGAMA',0,1,'C');
		$pdf->Cell(210,10,'UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA',0,1,'C');
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(200,7,'SATUAN PENGAWAS INTERNAL',0,1,'C');
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(200,2,'Jl. Jend. A. Yani 117 Telp./Fax. 031-8420118 60237 Website; http://spi.uinsby.ac.id Email; spi@uinsby.ac.id',0,1,'C');

		$pdf->Ln(10);
		$pdf->SetLineWidth(0);
    	$pdf->Line(10,32,200,32);
    	$pdf->SetLineWidth(1);
    	$pdf->Line(10,33,200,33);
    	$pdf->SetLineWidth(0);
    	$pdf->Line(10,34,200,34);
		$pdf->SetLineWidth(0);

		$pdf->Ln();
		$pdf->SetFont('Arial','',15);
		$pdf->Cell(200,7,'BERITA ACARA PEMERIKSAAN',0,1,'C');

		$pdf->Ln(10);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Pada hari ini, $hari tanggal $tanggal telah menghadap');


		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'kepada Pimpinan Satuan Pemeriksa Intern UIN Sunan Ampel sebagai berikut:');


		$pdf->Ln(15);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'NAMA :');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(2,2,':');
		$pdf->Cell(2,2,$row->nama);

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'NIP/NIM :');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': $nip');

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'JABATAN');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': $jabatan');

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'ALAMAT');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': $alamat');

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'No Telphone');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': $telpone');
		
		$pdf->Ln(15);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Memenuhi undangan Kepala SPI Nomor');
		$pdf->Cell(55);
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(10,2,$row->kode_undangan);

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Yang bersangkutan memberikan keterangan yang diakuinya adalah benar sebagai berikut:');

		$pdf->Ln(35);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Demikian berita acara ini dibuat untuk dipergunakan sebagaimana mestinya');

		$pdf->Ln(25);
		$pdf->Cell(135);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Surabaya,');
		$pdf->Cell(7);
		$pdf->Cell(10,2,$row->tgl_dibuat);

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Nama Terlapor');
		$pdf->Cell(115);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Kepala SPI');


		$pdf->Ln(20);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10,2,$row->nama);
		$pdf->Cell(115);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10,2,'Drs. Sutikno, M.Pd.I');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(10,2,'$nip');
		$pdf->Cell(115);
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(10,2,'196808061994031003');

		$pdf->Ln(20);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','bu',10);
		$pdf->Cell(10,2,'Saksi:');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'1. S. Khorriyatul Khotimah ');
		$pdf->Cell(80);
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(10,2,'(..........................................)');

		$pdf->Ln(30);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'2.	 Yuli Anggraini');
		$pdf->Cell(80);
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(10,2,'(..........................................)');


		$pdf->Output();

		echo json_encode(array("status" => TRUE));

		}
	}

	private function _do_upload()
	{
		$config['upload_path']          = 'assets/images/BAP/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 1000000000; //set max size allowed in Kilobyte
        $config['max_width']            = 1000000; // set max width image allowed
        $config['max_height']           = 1000000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('bap')) //upload and validate
        {
            $data['inputerror'][] = 'bap';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('kode_lapor') == '')
		{
			$data['inputerror'][] = 'kode_lapor';
			$data['error_string'][] = 'First name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('nama') == '')
		{
			$data['inputerror'][] = 'nama';
			$data['error_string'][] = 'Last name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('kode_undangan') == '')
		{
			$data['inputerror'][] = 'kode_undangan';
			$data['error_string'][] = 'Date of Birth is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('tgl_dibuat') == '')
		{
			$data['inputerror'][] = 'tgl_dibuat';
			$data['error_string'][] = 'Please select gender';
			$data['status'] = FALSE;
		}

		if($this->input->post('tgl_undangan') == '')
		{
			$data['inputerror'][] = 'tgl_undangan';
			$data['error_string'][] = 'Addess is required';
			$data['status'] = FALSE;
		}
		if($this->input->post('pukul') == '')
		{
			$data['inputerror'][] = 'pukul';
			$data['error_string'][] = 'Addess is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
