<!DOCTYPE html>
<html>
<?php $this->load->view('user/head') ?>
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Autocomplete</title>
  <link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery-ui1.css'?>">
</head>
  
<body class="hold-transition skin-blue sidebar-mini">
  <?php
    $tgl_lapor = date("Y-m-d");
  ?>
<div class="wrapper">

  <?php $this->load->view('user/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('user/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard User
        <small>Whistle Blowing System</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Main Navigation</li>
      </ol>
    </section>

    <section class="content-header">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h1 class="box-title">KIRIM PENGADUAN</h1>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form <?php echo form_open_multipart('user/input_unit/upload')?>  
               
              
              <div class="box-body">
                  <input type="text" name="kode_lapor" class="form-control" id="kode_lapor" value="<?= $kode_lapor; ?>" required="" readonly>
                  <input type="hidden" name="tgl_lapor" class="form-control pull-right" id="tgl_lapor" value="<?= $tgl_lapor; ?>" required="" readonly="">
                  <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $this->session->userdata('nim');?>" required="" readonly>
                  <input type="hidden" name="status" class="form-control" id="status" value="Belum Di Veriviaksi" required="" readonly>

                  <div class="form-group">
                <label>Nama Unit</label>
                <input type="text" name="nama_unit" class="form-control" id="title" placeholder="Nama Unit" >
                </div>
                 <div class="form-group">
                <label>Jenis Unit</label>
               <input type="text" name="jenis_unit" class="form-control" placeholder="Jenis Unit" readonly="" ></input>
                </div>


                <!-- select -->
                <div class="form-group">
                  <label>Jenis Pelanggaran</label>
<?php
                echo "<select name='pelanggaran' class='form-control' required> <option value='' disabled selected>------------------Pilih Jenis Pelangaran-------------------</option>";
                     foreach ($pelanggaran->result() as $row) {  
      echo "<option value='".$row->nama_pelanggaran."'>".$row->nama_pelanggaran."</option>";
      }
      echo "</select>";
?>
                </tr>
                  </select>
                </div>
                <div class="form-group">
                  <label>Detil Tempat Kejadian</label>
                  <textarea class="form-control" name="tempat_kejadian" rows="2" placeholder="Detil Tempat Kejadian" required=""></textarea>
                </div>
                <div class="form-group">
                <label>Tanggal Kejadian:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="date" name="tanggal" class="form-control pull-right" id="datepicker" required="">
                </div>
                <!-- /.input group -->
              </div>
                 <div class="form-group">
                  <label>Uraian Pengaduan</label>
                  <textarea class="form-control" name="uraian" rows="5" placeholder="Uraian Pengaduan" required=""></textarea>
                </div>
                <div class="form-group">
                  <label for="userfile">Upload File Bukti</label>
                  <input name="userfile" type="file" required="">
                  <p class="help-block">Tipe File (JPG, JPEG, PNG, DOC, DOCX, PPT, PDF, RAR, ZIP)</p>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" required=""> <font color="#ff0000"> Data Yang Saya Kirimkan Dapat Dipertanggung jawabkan </font> 
                  </label>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
      <!-- /.row -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
    <?php $this->load->view('user/footer') ?>
 

</body>
<!--  <script src="<?php echo base_url().'assets/assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script> -->
  <script src="<?php echo base_url().'assets/assets/js/bootstrap.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/assets/js/jquery-ui.js'?>" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function(){

        $('#title').autocomplete({
                source: "<?php echo site_url('user/input_unit/get_autocomplete');?>",
     
                select: function (event, ui) {
                    $('[name="nama_unit"]').val(ui.item.label); 
                    $('[name="jenis_unit"]').val(ui.item.description); 
                }
            });

    });
  </script>
</html>
