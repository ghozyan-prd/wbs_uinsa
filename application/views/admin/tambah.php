<!DOCTYPE html>
<html>
  <?php $this->load->view('admin/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('admin/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard admin
        <small>Whistle Blowing System</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Main Navigation</li>
      </ol>
    </section>

    <div class="col-md-10">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah User Pengawas</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form  class="form-horizontal" <?php echo form_open_multipart('admin/tambah/upload');?> 
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>

                  <div class="col-sm-10">
                    <input type="number" class="form-control" name="nip" id="nip" placeholder="Masukkan NIP anda" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="jabatan" required="">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                    <select name="level" type="email" class="form-control" required="">
                  <option disabled="disabled">Admin</option>
                  <option disabled="disabled">User</option>
                  <option value="2">Pengawas</option></select>
                  </div>
                </div>

                <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>


              <!-- /.box-body -->
              <!-- /.box-footer -->
            </form>
          </div>

          <!-- /.box -->
      <!-- /.row -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <?php $this->load->view('admin/footer') ?>
</body>
</html>
