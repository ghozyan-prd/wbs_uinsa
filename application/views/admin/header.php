<header class="main-header">
  <!-- Logo -->
  <a href="index2.html" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>W</b>BS</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Whistle Blowing</b></span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      <b> <script type="text/javascript">    
    //fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
    function tampilkanwaktu(){
        //buat object date berdasarkan waktu saat ini
        var waktu = new Date();
        //ambil nilai jam, 
        //tambahan script + "" supaya variable sh bertipe string sehingga bisa dihitung panjangnya : sh.length
        var sh = waktu.getHours() + ""; 
        //ambil nilai menit
        var sm = waktu.getMinutes() + "";
        //ambil nilai detik
        var ss = waktu.getSeconds() + "";
        //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
        document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
    }
</script>
<body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">								
<span id="clock"></span> 
<?php
$hari = date('l');
/*$new = date('l, F d, Y', strtotime($Today));*/
if ($hari=="Sunday") {
	echo "Minggu";
}elseif ($hari=="Monday") {
	echo "Senin";
}elseif ($hari=="Tuesday") {
	echo "Selasa";
}elseif ($hari=="Wednesday") {
	echo "Rabu";
}elseif ($hari=="Thursday") {
	echo("Kamis");
}elseif ($hari=="Friday") {
	echo "Jum'at";
}elseif ($hari=="Saturday") {
	echo "Sabtu";
}
?>,
<?php
$tgl =date('d');
echo $tgl;
$bulan =date('F');
if ($bulan=="January") {
	echo " Januari ";
}elseif ($bulan=="February") {
	echo " Februari ";
}elseif ($bulan=="March") {
	echo " Maret ";
}elseif ($bulan=="April") {
	echo " April ";
}elseif ($bulan=="May") {
	echo " Mei ";
}elseif ($bulan=="June") {
	echo " Juni ";
}elseif ($bulan=="July") {
	echo " Juli ";
}elseif ($bulan=="August") {
	echo " Agustus ";
}elseif ($bulan=="September") {
	echo " September ";
}elseif ($bulan=="October") {
	echo " Oktober ";
}elseif ($bulan=="November") {
	echo " November ";
}elseif ($bulan=="December") {
	echo " Desember ";
}
$tahun=date('Y');
echo $tahun;
?></b>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="user-image" alt="User Image">
            <span class="hidden-xs" ><?php echo $this->session->userdata('ses_nama');?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
             <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">

              <p>
                <?php echo $this->session->userdata('ses_nama');?>  <br> Whistle Blowing System
                <small>SPI UINSA</small>
              </p>
            </li>
            <!-- Menu Body --> 
              <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="<?php echo base_url().'index.php/Login/logout'?>"   class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>

  </nav>
</header>
