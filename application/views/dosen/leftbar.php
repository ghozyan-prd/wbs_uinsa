<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar dosen panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="user Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('nama');?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        <br>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                <i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
 <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
        <a href="home">
          <i class="fa fa-list-alt"></i> <span>Dashboard</span>
        </a>
      </li>

       <li>
        <a href="laporan_accepted">
          <i class="fa fa-user"></i> <span>Saya Terlapor</span>
        </a>
      </li>
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Tulis Pengaduan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="input"><i class="fa fa-circle-o"></i> Pengaduan Perorangan</a></li>
            <li><a href="input_unit"><i class="fa fa-circle-o"></i> Pengaduan Unit</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Riwayat Pengaduan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="data_perorangan"><i class="fa fa-circle-o"></i> Pengaduan Perorangan</a></li>
            <li><a href="data_unit"><i class="fa fa-circle-o"></i> Pengaduan Unit</a></li>
          </ul>
        </li>
       </li>
    </section>
    <!-- /.sidebar -->
  </aside>
