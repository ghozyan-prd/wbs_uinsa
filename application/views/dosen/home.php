<!DOCTYPE html>
<html>
  <?php $this->load->view('dosen/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('dosen/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('dosen/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1>
        بِسْــــــــــــــــــمِ اﷲِالرَّحْمَنِ اارَّحِيم
        <br><small>Selamat Datang Di WhistleBlowing System UINSA</small>
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Main Navigation</li>
      </ol>
    </section>
      <section id="contribution" class="section bg-image-2 contribution">
        
    </section>

    <section id="faq" class="section faq">
        <div class="container">
             <div class="row">
             <div class="col-md-12">
                    <h2>Kriteria Pengaduan</h2>
                </div>
            </div>
             
                    Jika Anda melihat atau mengetahui dugaan Tindak Pidana Korupsi atau bentuk pelanggaran lainnya yang dilakukan oleh Pegawai / Dosen UINSA, silahkan melapor ke Inspektorat SPI UINSA. <br> Jika laporan anda memenuhi syarat/kriteria, maka laporan Anda akan diproses lebih lanjut.

                    <ul>
                        <li>
                         <strong>WHAT</strong> yaitu apa perbuatan berindikasi Tindak Pidana Korupsi/pelanggaran yang diketahui.</li>
                        <li>
                        <strong>WHO</strong> yaitu siapa yang bertanggungjawab/terlibat dan terkait dalam perbuatan tersebut.</li>
                        <li>
                        <strong>WHERE</strong> yaitu dimana tempat terjadinya perbuatan tersebut dilakukan.</li>
                        <li>
                        <strong>WHEN</strong> yaitu kapan waktu perbuatan tersebut dilakukan.</li>
                         <li>
                        <strong>HOW</strong> yaitu Bagaimana cara perbuatan tersebut dilakukan (modus, cara, dan sebagainya).</li>
                        <li>
                        <strong>EVIDENCE (jika ada)</strong> yaitu Dilengkapi dengan bukti permulaan (data, dokumen, gambar dan rekaman) yang mendukung.</li>
                        <br>
                    </ul>



                <div class="row">
                <div class="col-md-12">
                    <h2>Jaminan Kerahasiaan dan Komitmen Kami</h2>
                </div>
            </div>
             <p>Anda tidak perlu khawatir terungkapnya identitas diri anda karena SPI UINSA akan MERAHASIAKAN & MELINDUNGI Identitas Anda sebagai whistleblower. SPI UINSA sangat menghargai <br> informasi yang Anda laporkan. Fokus kami kepada materi informasi yang Anda Laporkan. Sebagai bentuk terimakasih kami terhadap laporan yang Anda kirim, kami berkomitmen <br> untuk merespon laporan Anda selambat-lambatnya 7 (tujuh) hari kerja sejak laporan Anda dikirim.</p>

            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
      <!-- Info boxes -->
     
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('dosen/footer') ?>
</body>
</html>
