<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="bs-example">
    <h1 style='text-align: center;'><strong>Registrasi Akun Baru Non-Civitas UINSA</strong></h1>
     <h2 style='text-align: center;'>wishtle Blowing System</h2>
     <br>
 </br>
    <form  class="form-horizontal" <?php echo form_open_multipart('daftar/aksi_upload');?>
       <input type="hidden" name="level" value="3">
        <div class="form-group">
            <label class="control-label col-xs-2" for="inputEmail">Email:</label>
            <div class="col-xs-8">
                <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email Anda">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="inputPassword">Kata Sandi:</label>
            <div class="col-xs-8">
                <input type="password" name="pass" class="form-control" id="inputPassword" placeholder="Masukan Kata Sandi">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Nama Awal:</label>
            <div class="col-xs-8">
                <input type="text" name="nama" class="form-control" id="Namaawal" placeholder="Nama Awal Anda">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Nama Akhir:</label>
            <div class="col-xs-8">
                <input type="text" name="nama_akhir" class="form-control" id="Namaakhir" placeholder="Nama Akhir Anda">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="telp">No. Telp:</label>
            <div class="col-xs-8">
                <input type="tel" name="telp" class="form-control" id="telp" placeholder="Nomor Telepon / Handphone">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Alamat">Alamat:</label>
            <div class="col-xs-8">
                <textarea rows="8" name="alamat" class="form-control" id="Alamat" placeholder="Masukan Alamat Lengkap"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="KodePos">Kode Pos:</label>
            <div class="col-xs-8">
                <input type="text" name="pos" class="form-control" id="KodePos" placeholder="Kode Pos">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2">Jenis Kelamin:</label>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio"  name="jk" value="Laki-laki"> Laki-laki
                </label>
            </div>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio" name="jk" value="Perempuan"> Perempuan
                </label>
            </div>
        </div> 
         <div class="form-group">
            <label class="control-label col-xs-2" for="foto">Photo Profil:</label>
            <div class="col-xs-8">
                <input type="file" name="userfile">
            </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-8">
                <label class="checkbox-inline">
                    <input type="checkbox" value="Setuju" required>  Saya Setuju dengan <a href="#">Kebijakan dan Ketentuan</a> yang berlaku.
                </label>
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-8">
                <input type="submit" class="btn btn-primary" value="Kirim" onClick="return confirm('Apakah Anda Yakin?')">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div>
</body>
</html>                            