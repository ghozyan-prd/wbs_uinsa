<?php
class Product_model extends CI_Model{

	function product_list(){
		$hasil=$this->db->get('pelanggaran');
		return $hasil->result();
	}

	function save_product(){
		$data = array(
				'kode_pelanggaran' 	=> $this->input->post('kode_pelanggaran'), 
				'nama_pelanggaran' 	=> $this->input->post('nama_pelanggaran'), 
			);
		$result=$this->db->insert('pelanggaran',$data);
		return $result;
	}

	function update_product(){
		$kode_pelanggaran=$this->input->post('kode_pelanggaran');
		$nama_pelanggaran=$this->input->post('nama_pelanggaran');

		$this->db->set('nama_pelanggaran', $nama_pelanggaran);
		$this->db->where('kode_pelanggaran', $kode_pelanggaran);
		$result=$this->db->update('pelanggaran');
		return $result;
	}

	function delete_product(){
		$kode_pelanggaran=$this->input->post('kode_pelanggaran');
		$this->db->where('kode_pelanggaran', $kode_pelanggaran);
		$result=$this->db->delete('pelanggaran');
		return $result;
	}
	
}