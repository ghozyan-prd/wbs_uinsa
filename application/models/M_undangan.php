<?php
class M_undangan extends CI_Model {
    function kode_undangan() {
        $this->db->select('RIGHT(undangan.id_undangan,4) as kode', FALSE);
		  $this->db->order_by('id_undangan','DESC');    
		  $this->db->limit(1);    
		  $query = $this->db->get('undangan');      //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
		   //jika kode ternyata sudah ada.      
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		  }
		  else {      
		   //jika kode belum ada      
		   $kode = 1;    
		  }
		  $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "R/01/KP.01/VI/".$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
        }
      }
?>