<?php
class Model_laporan_unit extends CI_Model {
    function buat_kode() {
       $this->db->select('RIGHT(laporan_unit.id_laporan_unit,4) as kode', FALSE);
          $this->db->order_by('id_laporan_unit','DESC');    
          $this->db->limit(1);    
          $query = $this->db->get('laporan_unit');      //cek dulu apakah ada sudah ada kode di tabel.    
          if($query->num_rows() <> 0){      
           //jika kode ternyata sudah ada.      
           $data = $query->row();      
           $kode = intval($data->kode) + 1;    
          }
          else {      
           //jika kode belum ada      
           $kode = 1;    
          }
          $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
          $kodejadi = "KD-LP-2-2018-".$kodemax;    // hasilnya ODJ-9921-0001 dst.
          return $kodejadi;  
        }
      }
?>